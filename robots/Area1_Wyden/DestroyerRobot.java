package Area1_Wyden;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * DestroyerRobot - a robot by Joao Elias Ferraz Santana
 */
public class DestroyerRobot extends Robot
{
	int girar = 1; // Variavel usada para determinar o giro
	/**
	 * run: O robo gira ao redor de seu eixo, procurando um alvo
	 */
	public void run() {
		// Inicialização do robo

		setColors(Color.green,Color.yellow,Color.blue); // body,gun,radar

		// Robot main loop
		while(true) {
			//comportamentos
			turnRight(5 * girar);
			ahead(100);
			turnGunRight(360);
			turnLeft(85);
			ahead(100);
			turnRight(86);
			back(55);
			turnGunLeft(360);
		}
	}

	/**
	 * onScannedRobot: O que acontece ao ser detectado outro robo
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		if (e.getDistance() < 50 && getEnergy() > 50) {
			fire(2); // Se o outro robo está próximo, e ele tem muita vida, dispara intensamente!
		} 
		else {
			fire(1);
		}
		if (e.getBearing() >= 0) {
			girar = 1;
		} else {
			girar = -1;
		}
		
		turnRight(e.getBearing());
		ahead(e.getDistance() + 5);
		fire(1.255);
		back(75);
	}

	/**
	 * onHitByBullet: O que acontece ao ser atingido por um tiro
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		back(15);
		turnGunRight(360);
		fire(1.255);
	}
	
	/**
	 * onHitWall: O que acontece ao colodir com a parede
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		back(70);
		turnGunLeft(360);
	}	
	public void onWin(WinEvent event) {
		for(int i=1; i<=4; i++){ //danca da vitoria: atirar e, depois, girar a arma 90 graus anti-horario 4 vezes
			fire(1);
			turnGunLeft(90);
		}
	}
	public void onHitRobot(HitRobotEvent e) {
		if (e.getBearing() >= 0) { // verifica o angulo do oponente
			girar = 1; // e toma a direcao
		} else {
			girar = -1;
		}
		turnRight(e.getBearing());
		// Faz o calculo da intensidade necessaria para enfraquecer o oponente
		if (e.getEnergy() > 16) {
			fire(3);
		} else if (e.getEnergy() > 10) {
			fire(2);
		} else if (e.getEnergy() > 4) {
			fire(1);
		} else if (e.getEnergy() > 2) {
			fire(0.5);
		} else if (e.getEnergy() > .4) {
			fire(0.1);
		}
		ahead(40); // E avança para se chocar com ele
		back(30); //Um pequeno recuo
	}
}
