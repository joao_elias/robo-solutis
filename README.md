O presente robô, desenvolvido por mim, João Elias Ferraz Santana, realiza manobras de luta com o objetivo de realizar
ataques a um possivel rival e se defender do mesmo. Ele mescla estrategias de defesa e agressividade, reunindo os
melhores atributos das duas.
Como desvantagem, e possível mencionar a vulnerabilidade com relacao a um possivel rival que realize ataques executando
movimento de translacao circular uniforme.
O aspecto que mais me chamou a atencao durante o seu desenvolvimento foi o contato inicial com uma aplicacao que se
utiliza de inteligencia artificial para simular manobras de ataque e defesa. Ademais, e cabivel mencionar o excelente
aprendizado nas tecnicas de logica de programacao.